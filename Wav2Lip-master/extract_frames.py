import os
import cv2

def extract_frames(input_video, output_directory):

    """Extracts the unprocessed frames from the input video and saves them to the output directory, adding the file names to the concat.txt file.

    Args:
        input_video: The path to the input video.
        output_directory: The path to the output directory.
    """

    video_capture = cv2.VideoCapture(input_video)

    frame_number = 0
    with open(os.path.join(output_directory, "concat.txt"), "w") as concat_file:
        while True:

            ret, frame = video_capture.read()
            if not ret:
                break

            filename = f"{output_directory}/{frame_number}.jpg"
            cv2.imwrite(filename, frame)

            concat_file.write(f"file {filename}\n")

            frame_number += 1

    video_capture.release()

if __name__ == "__main__":

    input_video = "../inputs/kimk_7s_raw.mp4"
    output_directory = "../outputs/frames"

    extract_frames(input_video, output_directory)
